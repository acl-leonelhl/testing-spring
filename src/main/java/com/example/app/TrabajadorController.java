package com.example.app;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.app.entity.Trabajador;
import com.example.app.entity.TrabajadorDao;

@Controller
@RequestMapping("/trabajador")
public class TrabajadorController {

	@Autowired
	private TrabajadorDao trabajadorDao;
	
	@RequestMapping(value = { "/create/{nombre}" }, method = RequestMethod.GET)
	public String prueba(@PathVariable(value = "nombre") String nombre) {
		
		Trabajador trab = new Trabajador();
        trab.setName(nombre);
        trabajadorDao.save(trab);
        
		System.out.println("Se inserto: "+trab.getSlug());
		return "testing/insertar";
	}
}
